# OpenML dataset: ilpd

https://www.openml.org/d/1480

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Bendi Venkata Ramana, M. Surendra Prasad Babu, N. B. Venkateswarlu  
**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/ILPD+(Indian+Liver+Patient+Dataset)) - 2012  
**Please cite**: [UCI](https://archive.ics.uci.edu/ml/citation_policy.html)  

**Indian Liver Patient Dataset**  
This data set contains 416 liver patient records and 167 non liver patient records.The data set was collected from north east of Andhra Pradesh, India. The class label divides the patients into 2 groups (liver patient or not). This data set contains 441 male patient records and 142 female patient records. 

### Attribute Information  
V1. Age of the patient. Any patient whose age exceeded 89 is listed as being of age "90".  
V2. Gender of the patient  
V3. Total Bilirubin  
V4. Direct Bilirubin  
V5. Alkphos Alkaline Phosphatase  
V6. Sgpt Alanine Aminotransferase  
V7. Sgot Aspartate Aminotransferase   
V8. Total Proteins  
V9. Albumin  
V10. A/G Ratio Albumin and Globulin Ratio  

A feature indicating a train-test split has been removed.  

### Relevant Papers  
1. Bendi Venkata Ramana, Prof. M. S. Prasad Babu and Prof. N. B. Venkateswarlu, A Critical Comparative Study of Liver Patients from USA and INDIA: An Exploratory Analysis, International Journal of Computer Science Issues, ISSN:1694-0784, May 2012. 
2. Bendi Venkata Ramana, Prof. M. S. Prasad Babu and Prof. N. B. Venkateswarlu, A Critical Study of Selected Classification Algorithms for Liver Disease Diagnosis, International Journal of Database Management Systems (IJDMS), Vol.3, No.2, ISSN : 0975-5705, PP 101-114, May 2011.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1480) of an [OpenML dataset](https://www.openml.org/d/1480). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1480/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1480/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1480/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

